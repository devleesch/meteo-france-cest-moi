#include <Wire.h>
#include <SPI.h>
#include <HCSR04.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BMP280.h>

#include <ESP32_LoRaWAN.h>

#define RAIN_SENSOR 37
#define LIGHT_SENSOR 38
#define SOIL_SENSOR 39

#define SOIL_MIN 0
#define SOIL_MAX 3000

/* init sensor */
Adafruit_BMP280 bmp; // I2C

/*license for Heltec ESP32 LoRaWan, quary your ChipID relevant license: http://resource.heltec.cn/search */
uint32_t license[4] = { 0xD5397DF0, 0x8573F814, 0x7A38C73D, 0x48E68607 };

/* OTAA para*/
uint8_t DevEui[] = { 0x70, 0xB3, 0xD5, 0x7E, 0xD0, 0x05, 0x04, 0xB2};
uint8_t AppEui[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
uint8_t AppKey[] = { 0xE3, 0x95, 0xBC, 0x00, 0x36, 0x62, 0x69, 0x16, 0x24, 0xD8, 0x3F, 0x23, 0x12, 0xF1, 0x77, 0x4A };

/* ABP para*/
uint8_t NwkSKey[] = { 0x15, 0xb1, 0xd0, 0xef, 0xa4, 0x63, 0xdf, 0xbe, 0x3d, 0x11, 0x18, 0x1e, 0x1e, 0xc7, 0xda, 0x85 };
uint8_t AppSKey[] = { 0xd7, 0x2c, 0x78, 0x75, 0x8c, 0xdc, 0xca, 0xbf, 0x55, 0xee, 0x4a, 0x77, 0x8d, 0x16, 0xef, 0x67 };
uint32_t DevAddr = (uint32_t)0x007e6ae1;

/*LoraWan channelsmask, default channels 0-7*/
uint16_t userChannelsMask[6] = { 0x00FF, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000 };

/*the application data transmission duty cycle.  value in [ms].*/
uint32_t appTxDutyCycle = 15000;

/*OTAA or ABP*/
bool overTheAirActivation = true;

/*ADR enable*/
bool loraWanAdr = true;

/* Indicates if the node is sending confirmed or unconfirmed messages */
bool isTxConfirmed = true;

/* Application port */
uint8_t appPort = 2;

uint8_t confirmedNbTrials = 8;

uint8_t debugLevel = LoRaWAN_DEBUG_LEVEL;

/*LoraWan region, select in arduino IDE tools*/
LoRaMacRegion_t loraWanRegion = ACTIVE_REGION;

DeviceClass_t  loraWanClass = CLASS_A;


static void prepareTxFrame(uint8_t port) {

    Serial.println("----- Prepare -----");

    float raw_temperature = bmp.readTemperature();
    float raw_presure = bmp.readPressure();
    int altitude = bmp.readAltitude(1013.25);
    int light = digitalRead(LIGHT_SENSOR);
    int raw_soil = analogRead(SOIL_SENSOR);
    int rain = digitalRead(RAIN_SENSOR);

    Serial.print("Temperature : ");
    Serial.println(raw_temperature);

    Serial.print("Presure : ");
    Serial.println(raw_presure);

    Serial.print("Altitude : ");
    Serial.println(altitude);

    Serial.print("Distance : ");
 //   Serial.println(distance);

    Serial.print("Light : ");
    Serial.println(light);

    Serial.print("Rain : ");
    Serial.println(rain);

    int temperature = (raw_temperature + 30) * 10;
    int presure = (raw_presure / 100) - 300;

    int soil = map(raw_soil, SOIL_MIN, SOIL_MAX, 100, 0);

    Serial.print("Soil : ");
    Serial.println(soil);

    appDataSize = 6;  //AppDataSize max value is 64 ( src/Commissioning.h -> 128 )
    appData[0] = 0x00;
    appData[1] = altitude << 1 | rain;
    appData[2] = soil << 1 | light;
    appData[3] = temperature >> 2;
    appData[4] = temperature << 6 | presure >> 4;
    appData[5] = presure << 4;

    Serial.println("----- Prepare END -----");

}

void  downLinkDataHandle(McpsIndication_t *mcpsIndication)
{
    lora_printf("+REV DATA:%s,RXSIZE %d,PORT %d\r\n", mcpsIndication->RxSlot ? "RXWIN2" : "RXWIN1", mcpsIndication->BufferSize, mcpsIndication->Port);
    lora_printf("+REV DATA:");

    if (mcpsIndication->BufferSize > 0 && mcpsIndication->Buffer[0] == 1) {
      Serial.println("ON");
      digitalWrite(13, HIGH); // sets the digital pin 13 on

    } else if (mcpsIndication->BufferSize > 0 && mcpsIndication->Buffer[0] == 0) {
      Serial.println("OFF");
      digitalWrite(13, LOW);  // sets the digital pin 13 off
    }


    lora_printf("\r\n");
    Serial.println("DATA RECEIVED");

}



// Add your initialization code here
void setup() {
    Serial.begin(115200);
    pinMode(LIGHT_SENSOR, INPUT);
    pinMode(RAIN_SENSOR, INPUT);
    pinMode(13, OUTPUT);


    SPI.begin(SCK, MISO, MOSI, SS);
    Mcu.init(SS, RST_LoRa, DIO0, DIO1, license);
    deviceState = DEVICE_STATE_INIT;
    Display.init();

    while (!Serial || !bmp.begin(0x76))    {
        Serial.println("Could not find a valid BMP280 sensor, check wiring!");
        delay(2000);
    }

}

// The loop function is called in an endless loop
void loop() {

    switch (deviceState) {
        case DEVICE_STATE_INIT:
        {
            #if (LORAWAN_DEVEUI_AUTO)
                LoRaWAN.generateDeveuiByChipID();
            #endif
            LoRaWAN.init(loraWanClass, loraWanRegion);
            break;
        }
        case DEVICE_STATE_JOIN:
        {
            LoRaWAN.join();
            break;
        }
        case DEVICE_STATE_SEND:
        {
            prepareTxFrame(appPort);
            LoRaWAN.send(loraWanClass);
            deviceState = DEVICE_STATE_CYCLE;
            break;
        }
        case DEVICE_STATE_CYCLE:
        {
        // Schedule next packet transmission
            txDutyCycleTime = appTxDutyCycle + randr(-APP_TX_DUTYCYCLE_RND, APP_TX_DUTYCYCLE_RND);
            LoRaWAN.cycle(txDutyCycleTime);
            deviceState = DEVICE_STATE_SLEEP;
            break;
        }
        case DEVICE_STATE_SLEEP:
        {
            LoRaWAN.sleep(loraWanClass, debugLevel);
            break;
        }
        default:
        {
            deviceState = DEVICE_STATE_INIT;
            break;
        }
    }
}