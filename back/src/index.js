import express from 'express';
import mqtt from 'mqtt';

const app = express()

/* default stored value */
let data = {
    precipitation: 0,
    brightness: 0,
    soil: 0,
    rain: 0,
    altitude: 0,
    temperature: 0,
    pressure: 0,
}

/* -------------------- mqtt -------------------- */
const topic = "v3/devleesch-weather-station-v1@ttn/devices/numero2/up";
const topicDown = "v3/devleesch-weather-station-v1@ttn/devices/numero2/down/push";

const options = {
    username: 'devleesch-weather-station-v1@ttn',
    password: 'NNSXS.TSUPAKGRHTZR6FWPBEQ52PZJORWN4GOVSCSU4ZI.6ALXRJ5XYOSJ56TMXSKQNAYK67ZUND3SIWVDIP4KRQ5GEARYJHGA'
}

const client  = mqtt.connect('mqtt://eu1.cloud.thethings.network:1883', options)

client.on('connect', () => {
    client.subscribe(topic, function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log("connect on mqtt without error");
        }
    })
})

client.on('message', (topic, message) => {

    try {
        /** parse json to object */
        const obj = JSON.parse(message);

        /* access to value */
        const json = obj?.uplink_message?.decoded_payload ?? {};

        /* merge object */
        data = {...data, ...json}

        console.log(JSON.stringify(data))

    } catch (e) {
        console.log("bad json");
    }
})


/* -------------------- Express -------------------- */
const port = 3000;

app.get('/api', (req, res) => {
    res.send('Hello World!')
});

app.get('/api/update', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.send(data);
});

app.get('/api/relay/enable', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    console.log("api/relay/enable");

    client.publish(topicDown, JSON.stringify({downlinks: [{f_port: 1,frm_payload:"AQ==",priority: "NORMAL"}]}));

    res.send({message: "ok"})
});

app.get('/api/relay/disable', (req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    console.log("api/relay/disable");

    client.publish(topicDown, JSON.stringify({downlinks: [{f_port: 1,frm_payload:"AA==",priority: "NORMAL"}]}));

    res.send({message: "ok"})
})

app.listen(port, () => {
    console.log(`Server listening on port ${port}`)
});
