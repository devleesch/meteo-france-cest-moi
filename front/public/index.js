/** set loop to call api */
setInterval(() => {

    /** call route to update page */
    axios.get("/api/update", {})
        .then((r) => {
            console.log(r)

            /* update filed */
            document.getElementById('precipitation').innerText = r.data.precipitation
            document.getElementById('brightness').innerText = r.data.brightness
            document.getElementById('soil').innerText = r.data.soil
            document.getElementById('rain').innerText = r.data.rain
            document.getElementById('altitude').innerText = r.data.altitude
            document.getElementById('temperature').innerText = r.data.temperature
            document.getElementById('pressure').innerText = r.data.pressure

            console.log(r);
        })
        .catch(err => {
            console.log(err);
        })
}, 1000);

document.getElementById('button-enable').addEventListener('click', () => {
    console.log("button on clicked");

    axios.get("/api/relay/enable", {})
        .then(r => {
            console.log(r);
        }).cache(err => {
            console.log(err)
        }
    )
})

document.getElementById('button-disable').addEventListener('click', () => {
    console.log("button off clicked");

    axios.get("/api/relay/disable", {})
        .then(r => {
            console.log(r);
        }).cache(err => {
            console.log(err)
        }
    )
})